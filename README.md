# Building & Using
All you need to do is [install Nix](https://nixos.org/download.html) and `nix-build` in repo root.

Then we have to do some munging to get it working:

Slippi needs write access to where it is installed, so after successfully building, we have to move some files around.
```bash
# In repo root

# Copy all files in bin (including hidden files!)
cp -a result/bin bin2.2.5

# Because we copied from the nix-store, all the files
# are read-only. This means configs can't be saved, and
# we cannot put our user.json in the right place
chmod +w bin2.2.5
fd -H . bin2.2.5 --exec chmod +w

# Login
cp user.json bin2.2.5/User/user.json

# Launch! We have to explicitly call this instead of
# the wrapper script because the wrapper script references
# an absolute path to the nix-store. So if that is used,
# we cannot login due to nix-store being read-only.
./bin2.2.5/.dolphin-emu-wrapped
```

# Updating
1. Get a new rev from [`project-slippi/Ishiiruka`](https://github.com/project-slippi/Ishiiruka)
2. In `src` in `default.nix`, change the `rev` to it. Also change the `sha256` by replacing a few characters with `x`s or something:
3. 
```nix
src = fetchFromGitHub {
  owner = "project-slippi";
  repo = "Ishiiruka";
  rev = "2f71dc8193cdcc73cac9ca9f84198c9fb553d669"; # CHANGE THIS REV 
  sha256 = "0hgyghbd8kldp5z0b98q8xvxcslmr2h99zx20xw3w8afmv19wckf"; # MODIFY THIS SHA256 IN SOME WAY
};
```
3. `nix-build`. It will fail and complain about a sha256 mismatch. Take the sha is says it got, and replace the `sha256` in `src` with it.
4. `nix-build` again. This time it will successfully download & validate `src` and build!

# Controller Config
I just add this one line to my udev rules in my `configuration.nix`:

```nix
  services.udev.extraRules = ''
    # gamecube wii u usb adapter
    SUBSYSTEM=="usb", ENV{DEVTYPE}=="usb_device", ATTRS{idVendor}=="057e", ATTRS{idProduct}=="0337", MODE="0666"
  '';
```

# GPU Config
I play netplay on a Dell G5 5587, which has an NVIDIA graphics card w/Optimus Prime. I use it in sync mode, which means the laptop is always using it & not switching between it and the Intel card. I configured it in my `configuration.nix` like so:

```nix
  services.xserver.videoDrivers = [ "nvidia" ];
  hardware.nvidia.optimus_prime = {
    enable = true; #sync mode..may need to eventually change it to sync.enable = true

    # You can convert the value from `lspci` by:
    # * Stripping any leading zeros from the bus numbers or if the number is above 09, convert it to decimal and use that value.
    # * Replacing any full stops with colons.
    # * Prefix the final value with "PCI".

    # Bus ID of the NVIDIA GPU (You can find it using lspci, either under 3D or VGA)
    # `lspci` on my laptop says..
    # 01:00.0 VGA compatible controller: NVIDIA Corporation GP106M [GeForce GTX 1060 Mobile] (rev a1)
    nvidiaBusId = "PCI:1:0:0";

    # Bus ID of the Intel GPU. You can find it using lspci, either under 3D or VGA
    # `lspci` on my laptop says...
    # 00:02.0 VGA compatible controller: Intel Corporation UHD Graphics 630 (Mobile)
    intelBusId = "PCI:0:2:0";
  };
```