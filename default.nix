let pkgs-fork = "NixOS";
    pkgs-rev = "20.09";
    pkgs-sha256="1wg61h4gndm3vcprdcg7rc4s1v3jkm5xd7lw8r2f67w502y94gcy";
    mkPkgs = import (builtins.fetchTarball {
      url = "github.com/${pkgs-fork}/nixpkgs/archive/${pkgs-rev}.tar.gz";
      sha256 = pkgs-sha256;
    });
    pkgs = mkPkgs { overlays = [ ]; };
    slippi = with pkgs;
      dolphinEmuMaster.overrideAttrs(old: {
        pname = "ishiiruka";
        version = "2.2.5";

        buildInputs = old.buildInputs ++ [ gtk3 webkit ];
        nativeBuildInputs = [ wrapGAppsHook ];

        cmakeFlags = old.cmakeFlags ++ [
          "-DGTK3_GDKCONFIG_INCLUDE_DIR=${gtk3.out}/lib/gtk-3.0/include"
          "-DGTK3_GLIBCONFIG_INCLUDE_DIR=${glib.out}/lib/glib-2.0/include"
          "-DWEBKIT2_INCLUDE_DIR=${webkit.dev}/include/webkitgtk-4.0"
          "-DLINUX_LOCAL_DEV=true"
        ];

        # TODO: Change wrapper script to use relative path to wrapped
        # via $(dirname $(realpath $0))/.dolphin-emu-wrapped
        # ..Will need to sed $out/bin with that
        postInstall = old.postInstall + ''
            cp -R $src/Data/* $out/bin/
            touch $out/bin/portable.txt
        '';
  
        hardeningDisable = [ "all" ];
  
        src = fetchFromGitHub {
          owner = "project-slippi";
          repo = "Ishiiruka";
          rev = "v2.2.5";
          sha256 = "1qz8dnaa93f69rvl0pz6cll72g7q5yg7zjg26s1zppqfhb60rbxi";
        };

        patches = [
          # See https://gist.github.com/disassembler/9c4fad684600d60d9aba2733b315b639
          # and https://github.com/FasterMelee/Ishiiruka/issues/12
          # ./cubeb.patch
        ];
      });
in
slippi

